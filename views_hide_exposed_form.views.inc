<?php

/**
 * @file
 * Contains the Views Hide Exposed Form plugin.
 */

/**
 * Implements hook_views_plugins().
 */
function views_hide_exposed_form_views_plugins() {
  return array(
    'exposed_form' => array(
      'hidden' => array(
        'title' => t('Hidden'),
        'help' => t('Hidden exposed form'),
        'handler' => 'views_hide_exposed_form_plugin_exposed_form_hidden',
        'help topic' => 'exposed-form-hidden',
      ),
    ),
  );
}
