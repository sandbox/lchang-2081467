Views Hide Exposed Form

What
----
Adds an exposed form style: Hidden.

Why
---
For example, hide a filter, only use URL to change it.

How
---
-Install this module as usual.
-Set 'Exposed form style' to 'Hidden' in Views UI.

Who
---
http://drupal.org/user/2519980
