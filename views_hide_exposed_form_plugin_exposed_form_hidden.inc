<?php

/**
 * @file
 * Definition of views_hide_exposed_form_plugin_exposed_form_hidden.
 */

/**
 * Exposed form plugin that hides an exposed form.
 * @ingroup views_exposed_form_plugins
 */
class views_hide_exposed_form_plugin_exposed_form_hidden extends views_plugin_exposed_form {
  function render_exposed_form($block = FALSE) {
    parent::render_exposed_form($block);
  }
}
